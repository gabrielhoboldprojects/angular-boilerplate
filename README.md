# Angular Boilerplate

AngularJS 1 boilerplate utilizando gulp/browserify. Teses unitários com karma/jasmine.

### Execução: 
- Clone o repositório
- Digite **"npm install"** para instalar a dependências
- Digite **"gulp"** para inicializar o servidor local de desenvolvimento

Parâmetros possíveis para inicialização do servidor local: **gulp -[param]**

| Param | Descrição |
| ------ | ------ |
| -w | Watch - Faz o build do projeto ao detectar mudança no código  |
| -p | Utiliza configurações de produção (constantes etc...) |
| -m | Minifica os arquivos (.js e .css) |

##### Exemplos:
- build para desenvolvimento: **gulp -w**
- build para produção: **gulp -m -p**

### Execução dos testes
- Digite **"gulp unit"** para executar os testes uma única vez
- Digite **"gulp unit -w"** para executar os testes e esperar por mudanças (watch)