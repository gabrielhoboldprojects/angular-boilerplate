module.exports = {
    sourceDir: './app/',
    buildDir: './build/',
    scripts: {
        src: './src/app/**/*.js',
        dest: 'build/scripts',
        bundleName: 'app.js'
    },
    assets: {
        src: './src/assets/**/*',
        dest: 'build/assets'
    },
    styles: {
        src: './src/app/**/*.scss',
        dest: 'build/styles',
        bundleName: 'app.css'
    },
    views: {
        index: './src/app/index.html',
        src: './src/app/**/*.html',
        bundleName: 'tpls.js',
        templateModule: 'app'
    },
    fonts: {
        src: [''],
        dest: 'build/fonts'
    },
    constants: {
        src: './gulp/app.constants.json',
        dest: './src/app/app.constants.json'
    },
    test: {
        karma: './test/karma.conf.js',
        coverageDir: './coverage'
    }
};
