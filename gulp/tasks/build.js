'use strict';

const gulp = require('gulp');
const runSequence = require('run-sequence');

gulp.task('build', (cb) => {
    runSequence('clean', 'constants', ['sass', 'views', 'assets', 'fonts', 'browserify'],
        cb);
});
