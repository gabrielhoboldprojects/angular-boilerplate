'use strict';

const gulp = require('gulp');
const argv = require('yargs').argv;
const runSequence = require('run-sequence');
const del = require('del');
const fs = require('fs');

const config = require('../config');
const env = argv.p ? 'production' : 'development';

gulp.task('constants', () => {
    if (argv.w) {
        runSequence(['constants:watch']);
    }

    return del(config.constants.dest).then(() => {
        // It's not possible to use node require because of its cache
        let constants = JSON.parse(fs.readFileSync(config.constants.src, 'utf8'))[env];
        return fs.writeFile(config.constants.dest, JSON.stringify(constants, null, 2), 'utf-8');
    });
});

gulp.task('constants:watch', () => {
    return gulp.watch(config.constants.src, ['constants']);
});
