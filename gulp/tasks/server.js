'use strict';

const gulp = require('gulp');
const lite = require('lite-server');

gulp.task('server', ['build'], () => {
    lite.server();
});
