'use strict';

const gulp = require('gulp');
const glob = require('glob');
const browserify = require('browserify');
const watchify = require('watchify');
const argv = require('yargs').argv;
const gutil = require('gulp-util');
const source = require('vinyl-source-stream');
const streamify = require('gulp-streamify');
const uglify = require('gulp-uglify');

const config = require('../config');

gulp.task('browserify', () => {
    const files = glob.sync(config.scripts.src);
    const options = {
        entries: files,
        debug: true,
        cache: {},
        packageCache: {}
    };
    let b;

    if (argv.w) {
        b = watchify(browserify(options));
        b.on('update', bundle);
        b.on('log', gutil.log);
    } else {
        b = browserify(options);
    }

    return bundle();

    function bundle() {
        let bundle = b.bundle()
            .on('error', gutil.log.bind(gutil, 'Browserify Error'))
            .pipe(source(config.scripts.bundleName));

        if (argv.m) {
            bundle.pipe(streamify(uglify()));
        }

        return bundle.pipe(gulp.dest(config.scripts.dest));
    }
});
