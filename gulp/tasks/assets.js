'use strict';

const gulp = require('gulp');
const argv = require('yargs').argv;
const config = require('../config');
const runSequence = require('run-sequence');

gulp.task('assets', () => {
    if (argv.w) {
        runSequence(['assets:watch']);
    }
    return gulp.src(config.assets.src)
        .pipe(gulp.dest(config.assets.dest));
});

gulp.task('assets:watch', () => {
    return gulp.watch(config.assets.src, ['assets']);
});
