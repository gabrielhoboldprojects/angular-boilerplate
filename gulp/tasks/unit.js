'use strict';

const gulp = require('gulp');
const Server = require('karma').Server;
const argv = require('yargs').argv;
const path = require('path');
const del = require('del');

const config = require('../config');

/**
 * Run unit testes
 * @param -w Watch for changes
 */
gulp.task('unit', ['views', 'constants'], function (cb) {
    del(config.test.coverageDir)
        .then(() => {
            new Server({
                configFile: path.resolve(__dirname, '../..', config.test.karma),
                singleRun: !argv.w
            }, cb).start();
        });
});
