'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const argv = require('yargs').argv;
const runSequence = require('run-sequence');

const config = require('../config');

gulp.task('sass', () => {
    if (argv.w) {
        runSequence(['sass:watch']);
    }

    return gulp.src(config.styles.src)
        .pipe(sass({
            outputStyle: argv.m ? 'compressed' : 'nested'
        }).on('error', sass.logError))
        .pipe(concat(config.styles.bundleName))
        .pipe(gulp.dest(config.styles.dest));
});

gulp.task('sass:watch', () => {
    return gulp.watch(config.styles.src, ['sass']);
});
