'use strict';

const gulp = require('gulp');
const del = require('del');

const config = require('../config');

gulp.task('clean', (cb) => {
    return del([config.buildDir, config.test.coverageDir], cb);
});
