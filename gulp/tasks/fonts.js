'use strict';

const gulp = require('gulp');
const filter = require('gulp-filter');

const config = require('../config');

gulp.task('fonts', () => {
    return gulp.src(config.fonts.src)
        .pipe(filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(config.fonts.dest));
});
