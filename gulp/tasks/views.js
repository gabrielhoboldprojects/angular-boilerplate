'use strict';

const gulp = require('gulp');
const templateCache = require('gulp-angular-templatecache');
const argv = require('yargs').argv;
const runSequence = require('run-sequence');

const config = require('../config');

gulp.task('views', ['tpls'], () => {
    if (argv.w) {
        runSequence(['views:watch']);
    }

    return gulp.src(config.views.index)
        .pipe(gulp.dest(config.buildDir));
});

gulp.task('tpls', () => {
    return gulp.src([config.views.src, `!${config.views.index}`])
        .pipe(templateCache({
            filename: config.views.bundleName,
            module: config.views.templateModule
        }))
        .pipe(gulp.dest(config.scripts.dest));
});

gulp.task('views:watch', () => {
    return gulp.watch(config.views.src, ['views']);
});
