'use strict';

const istanbul = require('browserify-istanbul');

const karma = {

    basePath: '../',

    singleRun: true,

    frameworks: ['jasmine', 'browserify'],

    preprocessors: {
        'src/app/**/*.js': ['browserify'],
        'test/**/*.js': ['browserify']
    },

    browsers: ['PhantomJS'],

    reporters: ['progress', 'coverage'],

    autoWatch: true,

    browserify: {
        debug: true,
        extensions: ['.js'],
        transform: [
            istanbul({
                ignore: ['**/node_modules/**', '**/unit/**']
            })
        ]
    },

    urlRoot: '/__karma__/',

    files: [
        // app-specific code
        'src/app/**/*.js',

        // 3rd-party resources
        'node_modules/angular-mocks/angular-mocks.js',

        // test files
        'test/unit/**/*.js'
    ],

    coverageReporter: {
        type: 'html',
        dir: 'coverage/'
    }
};

module.exports = function (config) {
    config.set(karma);
};
