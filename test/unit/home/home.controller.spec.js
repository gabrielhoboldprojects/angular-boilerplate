'use strict';

describe('Unit: home.controller', function () {

    var $controller;

    beforeEach(angular.mock.module('app.home'));
    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_('home.controller');
    }));

    it('should exist', function () {
        expect($controller).toBeDefined();
    });

});
