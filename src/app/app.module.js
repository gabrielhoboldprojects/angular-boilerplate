'use strict';

var angular = require('angular');

angular.module('app', [
        require('angular-ui-router'),

        // App Modules
        require('./home/home.module')
    ])
    .config(require('./app.config'))
    .run(require('./app.run'))
    .constant('APPCONFIG', require('./app.constants.json'));
