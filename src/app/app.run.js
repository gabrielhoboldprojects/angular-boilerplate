'use strict';

appRun.$inject = ['$rootScope', '$state'];

function appRun($rootScope, $state) {
    function initiaze() {
        $rootScope.$state = $state;
    }

    initiaze();
}

module.exports = appRun;
