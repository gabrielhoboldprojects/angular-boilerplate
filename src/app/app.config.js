'use strict';

appConfig.$inject = ['$urlRouterProvider'];

function appConfig($urlRouterProvider) {
    $urlRouterProvider.otherwise('');
}

module.exports = appConfig;
