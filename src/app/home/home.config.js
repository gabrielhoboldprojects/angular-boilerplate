'use strict';

homeConfig.$inject = ['$stateProvider'];

function homeConfig($stateProvider) {
    $stateProvider
        .state('home', {
            abstract: true,
            template: '<ui-view/>'
        })
        .state('home.main', {
            url: '',
            templateUrl: 'home/home.html',
            controller: 'home.controller as vm',
            data: {
                pageTitle: 'Início'
            }
        });
}

module.exports = homeConfig;
