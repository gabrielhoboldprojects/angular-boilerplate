'use strict';

var angular = require('angular');

angular.module('app.home', [
        require('angular-ui-router')
    ])
    .config(require('./home.config'))
    .controller('home.controller', require('./home.controller'));

module.exports = 'app.home';
