'use strict';

const gulp = require('gulp');

require('./gulp');

/**
 * @description The default task will run the build tasks and start the server
 * @param -w Watch for file changes and rebuild the project
 * @param -m Minify the files (css and js)
 * @param -p Use production configurations
 * @example development(Watching): gulp -w
 * @example build for production: gulp build -p -m
 */
gulp.task('default', ['server']);
